# Patch Notes


## Compatibility
- Ready for v12 version.


## v12.331.1903.9
- Chat update : Thematization “Send to chat” windows

## v12.331.1903.8
- Update Monk's Active tile Triggers compatibility.
- fixed a few visual errors.

## v12.331.1903.6
- changed fonts for scene tab.
- Improved Journal visual.
- added a skin on some left sidebar buttons.
- improved visibility of sidebar selection tabs.
- fixed a few visual errors.

## v12.331.1903.5
- Added Monk's Active tile Triggers compatibility.
- Journal visual update.
- fixed a few visual errors.

## v12.331.1903.4
- fixed a few visual errors.

## v12.331.1903.3
- fixed a few visual errors.

## v12.331.1903.2
- Update v12 compatibility
- remove the old main theme code.
- fixed a few visual errors.

## v12.331.1903.1
- Update v12.331 compatibility
- Fixed the dark/light theme problem.
- Fixed the problem of the menu being displayed below the windows in the chats.
- Fixed some errors and added of others. 

## v12.328.1901.1
- initial v12 compatibility
- fix: button roll selector display
- Fixed some errors and added of others. 

## v11.315.1809.7
- Monk Navigation Scene skin added
- Fixed some errors and added of others.

## v11.315.1809.5
- Token Action HUD Classic skin added.
- Combat Tracker skin reworked.
- Fixed some errors and added of others.

## v11.315.1809.3
- Players window skin reworked.
- Smalltime skin added.
- H4 titles colored now.
- Fixed some errors and added of others.

## v11.315.1808.8
- Added Compatibility with Monk's Sound Enhancement.
- Improving visibility of different titles in journals.
- Improved visibility of dice icons in journals.
- Correction very large dice when a user visualizes the details of his roll.  

## v11.315.1808.7
- The Alternative Pause Icon module integration removed.
- Added Compatibility with Pin Cushion.
- Added New Game Paused.

## v11.315.1808.5
- Added Compatibility with Adventure Exporter
- Fixed some errors and added of others.

## v11.315.1808.3
- Improved Core code
- Updated Compatibility Starwarsffg v1.808.
- Fixed vehicles windows resize.
- Fixed some errors and added of others.

## v11.315.1807.1
- Improved Core code
- Updated Compatibility Starwarsffg v1.807.
- Disabled HOTFIX file by default.

## v11.315.1806.28
- The Alternative Pause Icon module has been integrated into the Ui.

## v11.315.1806.27
- Improved Core code
- Remove Status Halo integration.
- Added Compatibility with Quick Insert Module
- Added Compatibility with Spotlight Omniresearch Module
- Fixed some errors and added of others.

## v11.315.1806.26
- Changed color of directory item documentation for a better visibility
- Changed color of Setback dice for a better visibility
- Fixed missing code on the native token hub.

## v11.315.1806.25
- Improved visibility of skills tables in the sheets
- Fixed scrollbar rendering with last versions of Chrome.

## v11.315.1806.24
- Fixed edit content windows for weapons, Weapons vehicle, Armors, Gears & Species.
- Added in Game Details a hyperlink for  Report issue.
- Added in Game Details a hyperlink if you'd like to offer me a coffee !
- Fixed some errors and added of others.

## v11.315.1806.23
- Removed Compatibility with other systems.
- Changed Module name.
- Fixed visibility with third-party modules.
- Improved overall visual quality.
- Fixed some errors and added of others.

## v11.315.1806.22
- Updated Compatibility with Starwarsffg.
- Added Compatibility GM-Notes.
- Fixed some errors and added of others.

## v11.315.1806.20
- Improved overall visual quality.
- Fixed some errors and added of others.

## v11.315.1806.19
- Improved overall visual quality.
- Fixed some errors and added of others.

## v11.315.1806.17
- Significant performance improvements.
- Fixed some errors and added of others.

## v11.315.1806.15
- Improved Compatibility with Starwarsffg
- Improved Compatibility with Mandar Theme. (WIP)
- Added Compatibility with Monk's Enhanced Journal.
- Added Compatibility with Monk's Hotbar Expansion.
- Added Compatibility with Dice Tray.
- Tentative improve performance (reduce blur effect).
- Fixed some errors and added of others.

## v11.315.1806.12
- Improved Compatibility with Starwarsffg
- Improved Compatibility with Mandar Theme. (WIP)
- Fixed some errors and added of others.

## v11.315.1806.11
- Improved Compatibility with Starwarsffg
- Improved Compatibility with Mandar Theme. (WIP)
- Improved overall visual quality.
- Fixed blur effect application in the roll dialogue window.
- Fixed v2 sheet buttons for sheet elements and other windows.
- Fixed visual of SWFFG enhancement button.
- Added an animation glow on the buttons hover.
- Fixed some errors and added of others.

## v11.315.1806.10
- Improved Compatibility with Starwarsffg
- Improved Compatibility with Mandar Theme. (WIP)
- Added Compatibility with Monk's Tokenbar
- Fixed somes problems to scroll until the end of the sheets in the default theme.
- Updated Hotfix files
- Fixed some errors and added of others

## v11.315.1806.9
- Improved Compatibility with Starwarsffg
- Improved Compatibility with Mandar Theme. (WIP)
- Switching basic destiny window with Mandar destiny window.
- Added an option to correct problem to scroll until the end of the sheets in the default theme.
- Fixed font colours hard to see under the Mandar theme.
- Fixed issue [#3](https://gitlab.com/sasmira/space-op-ui/-/issues/3)
- Fixed some errors and added of others

## v11.315.1806.8
- Improved Compatibility with Starwarsffg
- Added Compatibility with Mandar Theme. (WIP)
- Fixed some errors and added of others

## v11.315.1806.7
- Updated Compatibility with Starwarsffg
- Added Compatibility with Mandar Theme. (WIP)

## v11.315.1806.7
- Updated Compatibility with Starwarsffg
- Fixed error in journals where images masked options. 

## v11.315.1806.6
- Updated Compatibility with Starwarsffg
- Fixed Compatibility with actor sheet v2
- Integrated Status Halo module.
- Added Compatibility with Dfreds Effects Panel
- Added Compatibility with Monk's Little Details.
- Added Compatibility with Automated Animations.
- Token Hud reworked.

## v11.315.1806.4
- Star Wars ffg ui rework done.

## v11.315.1806.3
- Star Wars ffg ui reworked (not finished)

## v11.315.1806.2
- Improved Core code
- Improved Star Wars FFG code
- Added Compatibility file.
- Added Compatibility with Modules Manager + 

## v11.315.1806.1
- Initial release
